﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace WeatherSample.Controllers
{
  public class WeatherController : Controller
  {
    // POST: Weather
    public JsonResult Api(WeatherApiParams parameters)
    {
      var url = @"https://deliverability.azurewebsites.net/api/Deliverability";

      var http = (HttpWebRequest)WebRequest.Create(new Uri(url));
      http.Accept = "application/json";
      http.ContentType = "application/json";
      http.Method = "POST";
      http.Headers["x-api-key"] = parameters.ApiKey; 

      var parsedContent = new JavaScriptSerializer().Serialize(parameters);
      ASCIIEncoding encoding = new ASCIIEncoding();
      Byte[] bytes = encoding.GetBytes(parsedContent);

      Stream newStream = http.GetRequestStream();
      newStream.Write(bytes, 0, bytes.Length);
      newStream.Close();

      var response = http.GetResponse();

      var stream = response.GetResponseStream();
      var sr = new StreamReader(stream);
      var content = sr.ReadToEnd();

      var result = new JavaScriptSerializer().Deserialize<WeatherApiResponse>(content);

      return Json(result, JsonRequestBehavior.AllowGet);
    }
  }

  public class WeatherApiParams
  {
    public string Carrier { get; set; }
    public string Service { get; set; }
    public string DestinationPostalCode { get; set; }
    public string OriginPostalCode { get; set; }
    public string OriginDateTime { get; set; }
    public string ApiKey { get; set; }
  }

  public class WeatherApiResponse
  {
    public WeatherApiResult Result { get; set; }
    public string Status { get; set; }
    public string StatusMessage { get; set; }
    public string RequestId { get; set; }
  }

  public class WeatherApiResult
  {
    public string RiskCategory { get; set; }
    public List<WeatherApiRoute> Route { get; set; }
  }
  public class WeatherApiRoute
  {
    public string CityState { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    public string ZipCode { get; set; }
    public string ZipCode3Digit { get; set; }
  }
}