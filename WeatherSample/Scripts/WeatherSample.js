﻿
$(document).ready(function() {

  //click the send button submits the form
  $('#btn-send').on('click', function(e) {
    e.preventDefault();

    var src = $('#btn-send');
    var frm = src.closest('FORM');

    $.ajax({
        url: '/Weather/Api',
        type: 'POST',
        data: {
          Carrier: $('#txt-carrier').val(),
          Service: $('#txt-service').val(),
          DestinationPostalCode: $('#txt-zipdest').val(),
          OriginPostalCode: $('#txt-ziporigin').val(),
          OriginDateTime: $('#txt-shipdate').val(),
          ApiKey: $('#txt-apikey').val()
        },
        beforeSend: function() {},
        success: function(data) {
          $('#weather-response').text(JSON.stringify(data, null, 2));
        },
        complete: function() {
        },
        error: function(request, status, error) {
          $('#weather-response').text(request.responseText);
        }
      });

  });

  //filters out services from non selected carriers
  $('#txt-carrier').on('change', function(e) {
    var value = $('#txt-carrier').val();
    var services = $('#txt-service');

    services.find('option').each(function() {
      $(this).attr('disabled', 'disabled');
    });
    services.find('option[data-carrier="'+value+'"]').each(function() {
      $(this).removeAttr('disabled');
    });
  });

  //triggers the above filter for the initial state
  $('#txt-carrier').trigger('change');

});