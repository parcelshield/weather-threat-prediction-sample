﻿using System.Web;
using System.Web.Optimization;

namespace WeatherSample
{
  public class BundleConfig
  {
    // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
    public static void RegisterBundles(BundleCollection bundles)
    {

      bundles.Add(new ScriptBundle("~/bundles/js").Include(
                "~/Scripts/jquery-1.8.3.js",
                "~/Scripts/bootstrap.js"));

      bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/site.css"));
    }
  }
}
